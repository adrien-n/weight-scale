# Some data for calibration

Calibration data used for my setup. This is provided for reference only and not
directly reproductible at the moment.

[[_TOC_]]

## Calibration

There is a linear relationship between the value given by the HX711 and the
weight on the load cells. The coefficient can be obtained with a few samples.
In my testing I went from 0kg to more than 13kg.

There is an artificial point at -3000g that I used to confirm the hypothesis that this was the right offset to use in this situation. As can be seen in the graph, it is merely extending the data to a point where the HX711 value is 0.

The following awk invocation processes the data in order to make it possible
to show the coefficient at each data point in addition to the raw experiment
data:

    awk -F' ' 'BEGIN { m = 0; v = 0 } { deriv = $2 / ($1 - v); v = $1; m += $2; printf("%d %d %f\n", $1, m, deriv * 50000); } END { printf("%d %d %f\n", v, m, 50000 * m / v) } END { printf("# Total weight: %d\n", m) }' < weight-and-value > weight-and-value.d


Plot with gnuplot: `plot 'weight-and-value.d' with lines, 'weight-and-value.d' using 1:3 with lines`

![Value plotted against weight showing perfect linear relation](value-against-weight.png "Value plotted against weight")


## Temperature coefficient

The resistance of load cells changes with load but also with the temperature.

The estimated coefficient is around 1000 HX711 units per °C (with HX711's gain
set to 64).

Data in 'temperature-and-value' is extracted from logs printed on the ESP32's
UART. Temperature was measured with a BME280.

Plot with gnuplot: `plot 'temperature-and-value' using 2:3 with lines`.

![Value plotted against temperature showing strong but noisy linear relation](value-against-temperature.png "Value plotted against temperature")

This is fairly noisy because the collection ran over several days but there are
several clean segments which make it possible to get the coefficient.

### Limitation: BME280 reacts more quickly than the load cells
The BME280 has far less inertia than the load cells (moreover the load cells
were in contact with a desk and combined the desk's inertia, and were subject
to less airflow). Typically, opening windows to change the house's air or
having the sun shine through windows had a much larger impact on the BME280's
output than on the HX711's one.

This reduces the usable range of data but there is still enough data to get a
good enough estimation and to reduce the effect of temperature from 100g/°C to
less than 20g/°C.

Plot with gnuplot: `plot 'temperature-and-value' using 1:2 with lines,
'temperature-and-value' using 1:($3/1000 - 4) with lines`. The second (green)
graph is not in °C; the goal here wasn't exactness but only showing the
correlation.

![Temperature and value plotted against time, showing temperature changes more and faster than the weight value](temperature-and-value-against-time.png "Temperature and value plotted against time")
