/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
   */

#include <stdio.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_sleep.h"
#include "esp_check.h"
#include "esp_vfs.h"
#include "esp_spiffs.h"

#include "driver/adc.h"
#include "esp_adc_cal.h"

#include <math.h>

#include "hx711.h"
#include "max7219.h"

#define SCALE_SPI_MOSI GPIO_NUM_25
#define SCALE_SPI_SCLK GPIO_NUM_32
#define SCALE_SPI_HOST HSPI_HOST
#define SCALE_MAX7219_SPI_CS GPIO_NUM_33

#define SCALE_HX711_DOUT GPIO_NUM_26
#define SCALE_HX711_PD_SCK GPIO_NUM_27

#define SCALE_THERMISTOR_ADC_CHANNEL ADC1_CHANNEL_6

static const char *TAG = "scale";

#ifdef _WITH_POWERSAVE
void show_time(void)
{
  struct timeval tv = {};
  gettimeofday(&tv, NULL);
  ESP_LOGI(TAG, "time: %lu.%lu", tv.tv_sec, tv.tv_usec);
}

static unsigned long
_get_time(void)
{
  struct timeval tv = {};

  gettimeofday(&tv, NULL);

  return tv.tv_sec;
}

void
go_to_sleep(void)
{
  unsigned long t_init = 3600 * 24;
  unsigned long t = t_init;
  unsigned long tv_sec = _get_time();

  ESP_LOGI(TAG, "Current time: %lu", tv_sec);

  if (t == t_init)
  {
    t = 3600;
  }

  ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(t * 1000 * 1000));
  ESP_LOGI(TAG, "Wakeup in %lu seconds.", t);
  show_time();
  ESP_ERROR_CHECK(esp_timer_dump(stdout));
  // rtc_gpio_isolate(GPIO_NUM_12);
  esp_deep_sleep_start();
}

static void
_light_sleep(unsigned long l)
{
  if (l == 0)
  {
    return;
  }

  ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(l * 1000 * 1000));
  ESP_LOGI(TAG, "Wakeup in %lu seconds.", l);

  esp_light_sleep_start();
}
#endif

#ifdef _WITH_BME280
static float
_bme280_init()
{
  bmp280_t bme280 = {};
  bmp280_params_t bme280_params = {};

  ESP_ERROR_CHECK(bmp280_init_desc(&bme280, BMP280_I2C_ADDRESS_0,
        I2C_NUM_0, GPIO_NUM_16, GPIO_NUM_17));
  ESP_ERROR_CHECK(bmp280_init_default_params(&bme280_params));
  ESP_ERROR_CHECK(bmp280_init(&bme280, &bme280_params));

  printf("BMP280: found %s\n", (bme280.id == BME280_CHIP_ID) ? "BME280" : "BMP280");

}

static int
_bme280_temp_get(bmp280_t * bme280, float * temperature)
{
  float pressure, temperature, humidity;

  if (bmp280_read_float(bme280, &temperature, &pressure, &humidity) != ESP_OK)
  {
    ESP_LOGW(TAG, "Temperature/humidity reading failed");
    return -1;
  }

  return 0;
}
#endif

static void
_thermistance_init(esp_adc_cal_characteristics_t * adc_cal_characteristics)
{
  ESP_LOGI(TAG, "ADC init");

  esp_adc_cal_value_t adc_cal_value =
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12,
        1100, adc_cal_characteristics);
  ESP_LOGI(TAG, "ADC cal characterize: %d", adc_cal_value);

  ESP_ERROR_CHECK(adc1_config_width(ADC_WIDTH_BIT_DEFAULT));
  ESP_ERROR_CHECK(adc1_config_channel_atten(SCALE_THERMISTOR_ADC_CHANNEL,
        ADC_ATTEN_DB_11));

  ESP_LOGI(TAG, "ADC init done");
}

static float
_thermistance_temp_get(esp_adc_cal_characteristics_t * adc_cal_characteristics)
{
  const float V0 = 3289;
  const float B = 3950;
  const float zero_celsius = 273.15;
  const float T0 = 25 + zero_celsius;
  const float R0 = 9810;
  const float r1 = 9840;

  uint32_t v;
  ESP_ERROR_CHECK(esp_adc_cal_get_voltage(ADC_CHANNEL_6, adc_cal_characteristics, &v));

  float vf = v;
  float r2 = r1 * vf / (V0 - vf);

  float temp = 1 / ((1 / T0) - (logf(R0 / r2) / B)) - zero_celsius;

  return temp;
}

static void
_hx711_init(hx711_t * hx711)
{
  hx711->dout = SCALE_HX711_DOUT;
  hx711->pd_sck = SCALE_HX711_PD_SCK;
  hx711->gain = HX711_GAIN_A_64;

  ESP_LOGI(TAG, "HX711 init");
  ESP_ERROR_CHECK(hx711_init(hx711));
  ESP_LOGI(TAG, "HX711 init done");
}

static int32_t
_hx711_weight_get(hx711_t * hx711, float temperature, int samples)
{
  int32_t hx711_data;
  int32_t weight;

  ESP_ERROR_CHECK(hx711_read_average(hx711, samples, &hx711_data));

  weight =
    (hx711_data
      + 3000 /* extrapolated after calibration */
      - (temperature - 21) * 950) /* load cell temperature compensation */
    * 4.4497 / 50 /* weight/value slope */
    - 1450 /* weight at 0 load (includes the scale itself) */
  ;

  return weight;

  /* NOTE: the 3000 and 1450 values above could be merged together but that
   * might make future calibrations more difficult. */
}

#ifdef _WITH_ST7789
static void
_lcd_init(TFT_t * dev, FontxFile fx32G[2])
{
  ESP_LOGI(TAG, "LCD: init");

  spi_master_init(dev, GPIO_NUM_23, GPIO_NUM_18, -1, GPIO_NUM_32, GPIO_NUM_33,
      GPIO_NUM_25);
  ESP_LOGI(TAG, "LCD: SPI init done");

  lcdInit(dev, 240, 240, 0, 0);
  ESP_LOGI(TAG, "LCD: LCD init done");

  lcdFillScreen(dev, BLACK);
  ESP_LOGI(TAG, "LCD: clear screen done");

  lcdSetFontDirection(dev, 0);
  ESP_LOGI(TAG, "LCD: font initialization");

  InitFontx(fx32G, "/spiffs/ILGH32XB.FNT", ""); // 16x32Dot Gothic

  ESP_LOGI(TAG, "LCD: init done");
}

static void
_lcd_disp(TFT_t * dev, FontxFile font[2], int32_t v)
{
  uint16_t xpos = 72;
  uint16_t ypos = 49 + 64;

  uint8_t buf[32] = {};
  snprintf((char*) buf, sizeof(buf), "%" PRId32, v);

  lcdDrawFillRect(dev, xpos, ypos - 31, xpos + 32 * 4, ypos - 31 + 24 * 2, BLACK);
  lcdDrawString(dev, font, xpos, ypos, buf, WHITE);
}

static void
_spiffs_directory(const char * path) {
  DIR* dir = opendir(path);
  assert(dir != NULL);
  while (true) {
    struct dirent*pe = readdir(dir);
    if (!pe) break;
    ESP_LOGI(__FUNCTION__,
        "d_name=%s d_ino=%d d_type=%x",
        pe->d_name, pe->d_ino, pe->d_type);
  }
  closedir(dir);
}

static void
_spiffs_init()
{
  esp_err_t ret;
  size_t total;
  size_t used;

  ESP_LOGI(TAG, "Initializing SPIFFS");

  esp_vfs_spiffs_conf_t conf = {
    .base_path = "/spiffs",
    .partition_label = NULL,
    .max_files = 10,
    .format_if_mount_failed =true
  };

  // Use settings defined above toinitialize and mount SPIFFS filesystem.
  // Note: esp_vfs_spiffs_register is anall-in-one convenience function.
  ret = esp_vfs_spiffs_register(&conf);

  if (ret != ESP_OK) {
    if (ret == ESP_FAIL) {
      ESP_LOGE(TAG, "Failed to mount or format filesystem");
    } else if (ret == ESP_ERR_NOT_FOUND) {
      ESP_LOGE(TAG, "Failed to find SPIFFS partition");
    } else {
      ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
    }
    return;
  }

  ret = esp_spiffs_info(NULL, &total, &used);
  if (ret != ESP_OK) {
    ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)",
        esp_err_to_name(ret));
  } else {
    ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
  }

  _spiffs_directory("/spiffs/");
}
#endif

static void
_max7219_init(max7219_t * max7219)
{
  spi_bus_config_t buscfg = {
    .mosi_io_num = SCALE_SPI_MOSI,
    .miso_io_num = -1,
    .sclk_io_num = SCALE_SPI_SCLK,
    .quadwp_io_num = -1,
    .quadhd_io_num = -1,
    .max_transfer_sz = 0,
    .flags = 0
  };

  ESP_LOGI(TAG, "MAX7219: init");

  ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &buscfg, SPI_DMA_CH_AUTO));

  ESP_LOGI(TAG, "MAX7219: SPI bus init done");

  max7219->cascade_size = 1;
  max7219->digits = 8;
  max7219->mirrored = true;

  ESP_ERROR_CHECK(max7219_init_desc(max7219, SCALE_SPI_HOST,
        SCALE_MAX7219_SPI_CS));
  ESP_ERROR_CHECK(max7219_init(max7219));
  ESP_ERROR_CHECK(max7219_set_decode_mode(max7219, true));
  ESP_ERROR_CHECK(max7219_set_brightness(max7219, MAX7219_MAX_BRIGHTNESS / 4));

  ESP_LOGI(TAG, "MAX7219: init done");
}

static void
_max7219_display(max7219_t * max7219, int32_t v)
{
  int i;

  if (v < 0)
  {
    v = -v;
  }

  ESP_ERROR_CHECK(max7219_clear(max7219));

  for (i = 0; i < max7219->digits; i++)
  {
    if (v == 0)
    {
      break;
    }
    max7219_set_digit(max7219, max7219->digits - i - 1, v % 10);
    v /= 10;
  }
}

void
app_main(void)
{
  bool first_sample = true;
  int32_t weight;
  float temperature;

#ifdef _WITH_ST7789
  _spiffs_init();

  TFT_t tft_dev;
  FontxFile fx32G[2];
  _lcd_init(&tft_dev, fx32G);
#endif

  max7219_t max7219 = {};
  _max7219_init(&max7219);

  esp_adc_cal_characteristics_t adc_cal_characteristics;
  _thermistance_init(&adc_cal_characteristics);

  hx711_t hx711 = {};
  _hx711_init(&hx711);

  while (1)
  {
    temperature = _thermistance_temp_get(&adc_cal_characteristics);
    weight = _hx711_weight_get(&hx711, temperature,
        (first_sample ? 1 : 32));

    ESP_LOGI(TAG, "Temperature: %.2f C, weight: %" PRId32, temperature, weight);
    _max7219_display(&max7219, weight);

    first_sample = false;
    vTaskDelay(500 / portTICK_RATE_MS);
  }

  /* Clean up */

  printf("Restarting now.\n");
  fflush(stdout);

  vTaskDelay(10 * 1000 / portTICK_PERIOD_MS);

  esp_restart();
}

