# Weight scale

A weight scale using an ESP32, an HX711, a 10K NTC thermistor and a MAX7219.
Accurate and stable.

Build with `idf.py all`, flash with `idf.py flash`.

## Table of contents

[[_TOC_]]

## Hardware

### ESP32-DevKitC-V4

Basic ESP32 board. Several pins are traps on ESP32 unfortunately but everything
can still be wired on one side of the board.

| Use | GPIO |
| --- | --- |
| SPI MOSI | 25 |
| SPI MISO | -1 |
| SPI SCLK | 32 |
| MAX7219 CS | 33 |
| HX711 DOUT | 26 |
| HX711 PD SCK | 27 |
| NTC Thermistor | 34 |

### HX711

I used a custom board that let me solder the four load cells wires directly.
Apart from that, any board will do. I'm using channel A with a gain of 64 but a
gain of 32 is probably enough and less noisy.

Source might have been lost. Not complicated fortunately. Can maybe be salvaged
at Aisler.net where I ordered the PCBs.

### Load cells

Four 50kg load cells bought on Aliexpress. Wires are fragile and annoying to
solder.

Load cells are wired in order to form a [Wheatstone
bridge](https://en.wikipedia.org/wiki/Wheatstone_bridge). This is
supposed to compensate for the influence of temperature on the load cell
resistance but this is not the case or at least not fully the case (see the
'calibration' directory for more details).

They are put in a basic 3D-printed holder. Source for the holders might have
been lost; they're definitely not complicated though. The only requirement is that the central part of the cells is not hampered.

### 10K NTC thermistor

Bought on Aliexpress. Base resistance is accurate at 5% and that's actually
huge. It seems that in practice it's accurate to 2%. Getting the actual value
is a must to avoid being several degrees off.

### MAX7219 7-segment display

Also bought on Aliexpress. Wired as SPI without MISO. Cheap enough and
effective because digits are big enough to be read from a distance.
